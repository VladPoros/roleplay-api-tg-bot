from dependency_injector import containers, providers

from Services.SyncStorageDataService import SyncDataService
from Services.StorageDataService import StorageDataService
from Commands.SyncDataCommand import SyncDataCommand
from Decorator.DataDecorator import DataDecorator
from Services.GoogleAPI.GoogleRemoteStorageService import GoogleRemoteStorageService
from Repositories.SqliteRepository.SqliteRepository import SqliteListRepository
from Repositories.MysqlRepository.MysqlListRepository import MysqlListRepository
from Services.TelegramBot.BotMessage import BotMessage
from Services.TelegramBot.TelegramBotService import TelegramBotService


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()

    """
        Insert data from specified DB from the desired service
    """
    GoogleApiService = providers.Factory(
        GoogleRemoteStorageService,
        credential_google=config.credential_google,
        spreadsheet_id=config.spreadsheet_id
    )

    DataDecorator = providers.Factory(DataDecorator)

    SqlListRepository = providers.Factory(MysqlListRepository)

    sync_data_service = providers.Factory(
        SyncDataService,
        remote_storage_service=GoogleApiService,
        decorator=DataDecorator,
        list_repository=SqlListRepository,
    )

    SyncDataCommand = providers.Factory(
        SyncDataCommand,
        service=sync_data_service
    )

    """
        Select desired data from specified DB by query
    """
    StorageDataDB = providers.Factory(MysqlListRepository)

    MessageRequest = providers.Factory(
        BotMessage,
        path_file=config.message_text_file
    )

    StorageDataService = providers.Factory(
        StorageDataService,
        list_repository=StorageDataDB
    )

    TelegramBot = providers.Factory(
        TelegramBotService,
        storage_data=StorageDataService,
        bot_message=MessageRequest,
        token=config.telegram_bot_token
    )
