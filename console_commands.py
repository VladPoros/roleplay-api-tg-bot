#!/usr/bin/python3

import argparse

from Commands.AbstractCommand import AbstractCommand
from Config import Application

parser = argparse.ArgumentParser()
parser.add_argument('--command', help='Command name')
args = parser.parse_args()

commands = {
    'syncData': Application.container.SyncDataCommand()
}


def get_command(name) -> AbstractCommand:
    return commands[name]


if args.command is not None:
    if args.command in commands:
        command = commands[args.command]
        command.handle()
    else:
        print('Please enter right command')
else:
    print('Please enter command')
