from .AbstractRemoteStorageService import AbstractRemoteStorageService
import httplib2
import apiclient
from oauth2client.service_account import ServiceAccountCredentials
from typing import Any


class GoogleRemoteStorageService(AbstractRemoteStorageService):
    __scopes = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
    __range = 'Лист1'
    __dimension = 'ROWS'

    def __init__(self, credential_google: str, spreadsheet_id: str):
        self.__credential_google = credential_google
        self.__spreadsheet_id = spreadsheet_id

    def build_google_service(self) -> Any:
        """Connection to Google Api"""

        credentials = ServiceAccountCredentials.from_json_keyfile_name(self.__credential_google, self.__scopes)
        httpAuth = credentials.authorize(httplib2.Http())
        service = apiclient.discovery.build('sheets', 'v4', http=httpAuth)
        return service

    def get_data(self) -> list:
        """Get data from sheet"""

        service = self.build_google_service()
        info_table = service.spreadsheets().values().get(
            spreadsheetId=self.__spreadsheet_id,
            range=self.__range,
            majorDimension=self.__dimension
        ).execute()
        data_table = info_table['values']
        return data_table
