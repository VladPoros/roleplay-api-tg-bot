from abc import ABC, abstractmethod


class AbstractRemoteStorageService(ABC):

    @abstractmethod
    def get_data(self) -> list:
        pass
