import json


class BotMessage:
    __state_message = []

    def __init__(self, path_file: str):
        self.__data_json = json.load(open(path_file, mode='r'))
        self.__state_message = self.__data_json

    def get_message(self, message) -> str:
        """Get specific bot answer for user`s message"""

        return self.__state_message[message]
