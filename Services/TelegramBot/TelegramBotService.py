from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from Services.StorageDataService import StorageDataService
from .BotMessage import BotMessage


class TelegramBotService:
    __name_row = ''

    def __init__(self, bot_message: BotMessage, storage_data: StorageDataService, token: str):
        self.__bot_message = bot_message
        self.__storage_data = storage_data
        self.__token = token

    def text_handler(self, update, context):
        update.message.reply_text(self.__bot_message.get_message('text_handler'))

    def search_handler(self, update, context) -> str:
        update.message.reply_text(self.__bot_message.get_message('search_handler'))
        return self.__name_row

    def name_row_handler(self, update, context) -> ConversationHandler.END:
        """Give result from query by searching name (name_row) from user in chat"""

        context.user_data[self.__name_row] = update.message.text
        data = self.__storage_data.search(update.message.text)
        if data:
            for name, value in data:
                update.message.reply_text(name + ' - ' + value)
        else:
            update.message.reply_text(self.__bot_message.get_message('error_name_handler'))
        return ConversationHandler.END

    def cancel_handler(self, update, context) -> ConversationHandler.END:
        update.message.reply_text(self.__bot_message.get_message('cancel_handler'))
        return ConversationHandler.END

    def main(self):
        updater = Updater(self.__token, use_context=True)
        dispatcher = updater.dispatcher

        conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('search', self.search_handler)],
            states={
                self.__name_row: [
                    MessageHandler(Filters.text & (~ Filters.command), self.name_row_handler,
                                   pass_user_data=True)
                ]
            },
            fallbacks=[CommandHandler('cancel', self.cancel_handler)],
        )

        dispatcher.add_handler(conversation_handler)
        dispatcher.add_handler(MessageHandler(Filters.text, self.text_handler))

        updater.start_polling()
        updater.idle()
