from Repositories.AbstractRepository import AbstractRepository
from Decorator.AbstractDataDecorator import AbstractDataDecorator
from Services.GoogleAPI.AbstractRemoteStorageService import AbstractRemoteStorageService


class SyncDataService:

    def __init__(self,
                 remote_storage_service: AbstractRemoteStorageService,
                 decorator: AbstractDataDecorator,
                 list_repository: AbstractRepository
                 ):
        self.__list_repository_service = list_repository
        self.__decorator_service = decorator
        self.__remote_data_service = remote_storage_service

    def synchronization_data(self) -> None:
        """Insert data from Google Sheets to DB"""
        self.__list_repository_service.truncate()
        prepared_data = self.__decorator_service.handle(self.__remote_data_service.get_data())
        self.__list_repository_service.insert(['name', 'value'], prepared_data)
