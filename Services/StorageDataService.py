from Repositories.AbstractRepository import AbstractRepository


class StorageDataService:
    def __init__(self, list_repository: AbstractRepository):
        self.__list_repository = list_repository

    def search(self, data) -> list:
        self.__list_repository.find_by_name(data)
        return self.__list_repository.get_state()
