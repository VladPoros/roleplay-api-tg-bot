from abc import ABC, abstractmethod


class AbstractCommand(ABC):
    @abstractmethod
    def handle(self):
        pass
