from Commands.AbstractCommand import AbstractCommand
from Services.SyncStorageDataService import SyncDataService


class SyncDataCommand(AbstractCommand):

    def __init__(self, service: SyncDataService):
        self.service = service

    def handle(self) -> None:
        self.service.synchronization_data()
