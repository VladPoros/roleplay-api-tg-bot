from Decorator.AbstractDataDecorator import AbstractDataDecorator


class DataDecorator(AbstractDataDecorator):
    @staticmethod
    def handle(data: list) -> list:
        """
        Method transforms data from Google Sheets for DB
        :param data: [[]]
        :return: [()]
        """

        list_row_values = []
        data.pop(0)
        for list_data in data:
            if len(list_data) == 2 and list_data[0] != '':
                storage_data = (list_data[0], list_data[1])
                list_row_values.append(storage_data)
        return list_row_values
