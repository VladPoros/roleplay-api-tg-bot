from abc import ABC, abstractmethod


class AbstractDataDecorator(ABC):
    @staticmethod
    @abstractmethod
    def handle(data: list) -> list:
        pass
