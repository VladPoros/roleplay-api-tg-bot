from __future__ import annotations
from abc import abstractmethod
from Database.MysqlDatabase import MysqlDatabase
import re
from Repositories.AbstractRepository import AbstractRepository


class BaseMysqlRepository(AbstractRepository):
    _state = []
    _where_state = []
    _select_columns_state = ['*']

    def __init__(self):
        self.database = MysqlDatabase()

    @abstractmethod
    def get_name_table(self) -> str:
        pass

    def select_columns(self, columns=None) -> BaseMysqlRepository:
        """Collect columns for query to DB"""

        if columns is not None:
            self._select_columns_state = columns
        return self

    def where(self, column_name, value, operator='=') -> BaseMysqlRepository:
        """Collect parameters for query WHERE to DB"""

        self._where_state.append({
            'column_name': column_name,
            'value': value,
            'operator': operator
        })
        return self

    def truncate(self):
        self.database._get_cursor().execute("TRUNCATE TABLE " + self.get_name_table())

    def insert(self, columns: list, data: list):
        columns = ','.join(columns)
        values = re.sub('[A-Za-z]+', '%s', columns)
        query = f"INSERT INTO {self.get_name_table()} ({columns}) VALUES ({values})"
        self.database._get_cursor().executemany(query, data)

    def build_select_columns(self) -> str:
        select_columns = ','.join(self._select_columns_state)
        return f" {select_columns} FROM {self.get_name_table()}"

    def build_wheres(self) -> str:
        part_query = ""
        for index, where in enumerate(self._where_state):
            if index == 0:
                operator = 'WHERE'
            else:
                operator = 'AND'
            part_query += f" {operator} {where['column_name']} {where['operator']} '{where['value']}'"
        return part_query

    def build_query(self) -> str:
        query = "SELECT"
        query += self.build_select_columns()
        query += self.build_wheres()
        return query

    def fetch(self):
        """Database query to get the desired data from the table"""

        self.database._get_cursor().execute(self.build_query())
        result = self.database._get_cursor().fetchall()
        self._state = result
        self.clear()

    def clear(self) -> None:
        """Clear where state and column state for correct re-call query"""

        self._where_state.clear()
        self._select_columns_state.clear()

    def _init_state(self) -> None:
        if self._state is None:
            self._state = []

    def get_state(self) -> list:
        """Return data from query to DB"""

        self._init_state()
        return self._state
