from abc import ABC, abstractmethod


class AbstractRepository(ABC):

    @abstractmethod
    def truncate(self):
        pass

    @abstractmethod
    def insert(self, columns: list, data: list):
        pass

    @abstractmethod
    def find_by_name(self, name) -> list:
        pass

    @abstractmethod
    def get_state(self) -> list:
        pass
