from Repositories.SqliteRepository.BaseSqliteRepository import BaseSqliteRepository


class SqliteListRepository(BaseSqliteRepository):
    __table_name = 'information'

    def get_name_table(self):
        return self.__table_name

    def find_by_name(self, name) -> list:
        self.select_columns(['name', 'value'])
        self.where('name', name + '%', 'LIKE')
        return self.fetch()
