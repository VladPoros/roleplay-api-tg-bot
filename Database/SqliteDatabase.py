import sqlite3

from .AbstractDatabase import AbstractDatabase


class SqliteDatabase(AbstractDatabase):

    def __init__(self):
        self.__connection = sqlite3.connect('data.sqlite', check_same_thread=False)
        self.__cursor = self.__connection.cursor()

    def _get_cursor(self) -> sqlite3.Cursor:
        return self.__cursor

    def __del__(self):
        self.__connection.commit()
