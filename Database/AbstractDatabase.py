from abc import ABC, abstractmethod


class AbstractDatabase(ABC):

    @abstractmethod
    def _get_cursor(self):
        pass
