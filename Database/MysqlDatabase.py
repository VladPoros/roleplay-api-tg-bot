import mysql.connector
from mysql.connector.cursor_cext import CMySQLCursor
from Database.AbstractDatabase import AbstractDatabase
import os
from dotenv import load_dotenv

load_dotenv()


class MysqlDatabase(AbstractDatabase):

    def __init__(self):
        self.__connection = mysql.connector.connect(
            host=os.getenv('DB_HOST'),
            user=os.getenv('DB_USERNAME'),
            password=os.getenv('DB_PASSWORD'),
            database=os.getenv('DB_DATABASE'),
        )
        self.__cursor = self.__connection.cursor()

    def _get_cursor(self) -> CMySQLCursor:
        return self.__cursor

    def __del__(self):
        self.__connection.commit()
