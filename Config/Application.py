from DependencyInjector.Container import Container

container = Container()

container.config.credential_google.from_env('CREDENTIAL_GOOGLE')
container.config.spreadsheet_id.from_env('SPREADSHEET_ID')

container.config.message_text_file.from_env('TELEGRAM_MESSAGES_FILE')
container.config.telegram_bot_token.from_env('TOKEN_TELEGRAM_BOT')