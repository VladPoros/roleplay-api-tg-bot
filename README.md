## Description project

This project is Telegram Bot, which working with database. <br />
By command **'/search'** Bot give info to user for need word from user`s message. <br />

Used two database: MySQL and SQLite.<br />

Data for table in database is take from Google Api(Google Sheets).

## Install project and working process of project

For install project clone my repository.<br />
Use **Python 3.8**<br />
Install all libs from **requirements.txt** (pip install -r requirements.txt) <br />

Then look at the file **env.example**.<br />
In this file you find all required variables for start this project.<br />

Block of DATABASE need for connection and work with database query.<br />

Block of GOOGLE_API need for connection to Google Api and handle data from sheets.
CREDENTIAL_GOOGLE you have in your service account in Google Developers Console.
SPREADSHEET_ID is id for your needed document.<br />

Block of TELEGRAM for work with Telegram Bot.
TOKEN_TELEGRAM_BOT is token from BotFather in Telegram when you will create own bot.
TELEGRAM_MESSAGES_FILE this is file with message from bot.

## Launch project

Project work by Container of Dependency Injector. In Container you can change database or service of synchronization data.<br />

For synchronization data use file **console_commands.py** via terminal, where point name of command, or via CRON.<br />
For start to work bot use file **main.py** via terminal.